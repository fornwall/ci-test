# Installing meeshkan for use in GitLab CI
The [.gitlab-ci.yml](.gitlab-ci.yml) file shows how to install the [meeshkan](https://github.com/meeshkan/meeshkan/) tool for use in the GitLab CI environment.

See the [Pipelines page](https://gitlab.com/fornwall/ci-test/pipelines/) for the log output.
